package com.fomin;

import com.fomin.models.MyClass;
import com.fomin.models.Calculate;
import com.fomin.models.Bank;
import com.fomin.reflections.MyReflection;
import com.fomin.reflections.CalculateReflection;
import com.fomin.reflections.BankReflection;

public class Main {
    public static void main(String[] args) {
        System.out.println(BankReflection.printAnnotatedFields(new Bank()));
        CalculateReflection.invokeOverloadMethods(new Calculate());
        MyReflection.invokerMyMethods(new MyClass());
    }
}
