package com.fomin.reflections;

import com.fomin.models.MyClass;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class MyReflection {

    public static void invokerMyMethods(MyClass myClass) {
        Class<MyClass> c = MyClass.class;
        try {
            Method method1 = c.getDeclaredMethod("myMethod", String.class, int[].class);
            Method method2 = c.getDeclaredMethod("myMethod", String[].class);
            method1.invoke(myClass, "PrivatBank is national", new int[] {9, 9, 9});
            method2.invoke(myClass, new Object[] {new String[] {"6", "5", "4"}});
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }
}
