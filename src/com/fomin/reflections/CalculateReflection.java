package com.fomin.reflections;

import com.fomin.models.Calculate;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class CalculateReflection {

    public static void invokeOverloadMethods(Calculate calculate) {
        Class<Calculate> c = Calculate.class;
        try {
            Method intMethod = c.getDeclaredMethod("calculate", int.class, int.class);
            System.out.println(intMethod.invoke(calculate, 10000, 3280));
            Method floatMethod = c.getDeclaredMethod("calculate", float.class, float.class);
            System.out.println(floatMethod.invoke(calculate, 123.4F, 567.8F));
            Method doubleMethod = c.getDeclaredMethod("calculate", double.class, double.class);
            System.out.println(doubleMethod.invoke(calculate, 987.6, 543.27));
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }
}
