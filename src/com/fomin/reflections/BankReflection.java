package com.fomin.reflections;

import com.fomin.annotations.Annotation;
import com.fomin.models.Bank;

import java.lang.reflect.Field;

public class BankReflection {
    public static Bank printAnnotatedFields(Bank bank) {
        Class<Bank> c = Bank.class;
        Field[] fields = c.getDeclaredFields();
        for (Field f : fields) {
            if (f.isAnnotationPresent(Annotation.class)) {
                f.setAccessible(true);
                Annotation annotation = f.getAnnotation(Annotation.class);
                try {
                    f.set(bank, annotation.value());
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }
        return bank;
    }
}
