package com.fomin.models;

import com.fomin.annotations.Annotation;

public class Bank {

    @Annotation("Privat")
    private String name;
    private int account;

    @Annotation("national")
    private String status;

    public Bank() {}

    public Bank(String name, int account, String status) {
        this.name = name;
        this.account = account;
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAccount() {
        return account;
    }

    public void setAccount(int account) {
        this.account = account;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Bank{" +
                "name='" + name + '\'' +
                ", account=" + account +
                ", status='" + status + '\'' +
                '}';
    }
}
