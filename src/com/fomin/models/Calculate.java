package com.fomin.models;

public class Calculate {
    public int calculate(int x, int y) {
        return x + y;
    }
    public double calculate(double x, double y) {
        return x + y;
    }
    public float calculate(float x, float y) {
        return x + y;
    }
}
